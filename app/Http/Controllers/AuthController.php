<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AuthController extends ApiController
{
    protected $client;

    /**
     * AuthController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);

        $this->client = DB::table('oauth_clients')
                          ->where(['password_client' => 1])
                          ->first();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(Request $request)
    {
        $request->request->add([
            'username' => $request->get('email'),
            'password' => $request->get('password'),
            'grant_type' => 'password',
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
        ]);

        // Implemented as temporary solution as wasn't sure how you wanted auth doing and wasn't in spec
        if (Auth::attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password')])) {
            $user = Auth::user();
            $token = $user->createToken('authtoken')->accessToken;
            return response()->json(['token' => $token]);
        }
    }
}
