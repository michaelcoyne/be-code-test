<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Organisation;
use App\Services\OrganisationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

/**
 * Class OrganisationController
 * @package App\Http\Controllers
 */
class OrganisationController extends ApiController
{
    /**
     * @param Request $request
     * @param OrganisationService $service
     * @return JsonResponse
     */
    public function store(Request $request, OrganisationService $service): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'subscribed' => 'integer',
        ]);

        if ($validator->fails()) {
            return $this->respond($validator->errors(), 422);
        }

        /** @var Organisation $organisation */
        $organisation = $service->createOrganisation($request->all());

        // should queue this - don't have time to configure mail locally
        /*try {
            Mail::send('email', ['organisation' => $organisation], function ($m) use ($organisation) {
                $m->from(Auth::user()->email, 'Your Application');
                $m->to(Auth::user()->email, Auth::user()->name)->subject('Organisation created');
            });
        } catch (Exception $e) {

        }*/

        return $this
            ->transformItem('organisation', $organisation, ['user'])
            ->respond();
    }

    /**
     * @param Request $request
     * @param OrganisationService $service
     * @return JsonResponse
     */
    public function listAll(Request $request, OrganisationService $service)
    {
        $filter = $request->get('filter') ?? '';
        $organisationsArray = $service->listOrganisations($filter);

        return $this
            ->transformCollection('organisations', $organisationsArray)
            ->respond();
    }
}
