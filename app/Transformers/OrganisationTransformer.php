<?php

declare(strict_types=1);

namespace App\Transformers;

use App\Organisation;
use League\Fractal\TransformerAbstract;
use App\User;

/**
 * Class OrganisationTransformer
 * @package App\Transformers
 */
class OrganisationTransformer extends TransformerAbstract
{

    protected $defaultIncludes = [
        'user'
    ];

    /**
     * @param Organisation $organisation
     * @return array
     */
    public function transform(Organisation $organisation): array
    {
        return $organisation->toArray();
    }

    /**
     * @param Organisation $organisation
     * @return object
     */
    public function includeUser(Organisation $organisation) : object
    {
        $user = User::find($organisation->owner_user_id);
        return $this->item($user, new UserTransformer($user));
    }
}
