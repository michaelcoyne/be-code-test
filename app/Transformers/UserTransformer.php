<?php
/**
 * Created by PhpStorm.
 * User: appleuser
 * Date: 2019-12-22
 * Time: 23:31
 */

namespace App\Transformers;
use League\Fractal\TransformerAbstract;
use App\User;

/**
 * Class UserTransformer
 * @package App\Transformers
 */
class UserTransformer extends TransformerAbstract
{

    /**
     * @param User $user
     * @return array
     */
    public function transform(User $user): array
    {
        return [$user];
    }

}