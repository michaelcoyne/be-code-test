<?php

declare(strict_types=1);

namespace App\Services;

use App\Organisation;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * Class OrganisationService
 * @package App\Services
 */
class OrganisationService
{
    /**
     * @param array $attributes
     * @return Organisation
     */
    public function createOrganisation(array $attributes): Organisation
    {
        $organisation = new Organisation();
        $attributes['owner_user_id'] = Auth::user()->id;
        $attributes['trial_end'] = Carbon::now()->addDays(30)->toDateTimeString();
        $attributes['subscribed'] = 0;
        $organisation->fill($attributes)->save();
        return $organisation;
    }


    /**
     * @param string $filter
     * @return array
     */
    public function listOrganisations(string $filter): array
    {
        $allowedFilters = ['subbed', 'trail'];

        if (!in_array($filter, $allowedFilters)) {
            $orgCollection = Organisation::all();
        } else {
            $subscribed = $filter === 'subbed' ? 1 : 0;
            $orgCollection = Organisation::where('subscribed', $subscribed)->get();
        }

        return $orgCollection->all();
    }
}
